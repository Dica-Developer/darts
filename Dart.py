import cv2
import logging
import numpy as np
import threading
import time
import utils.Math as Math

LOG_DEBUG = logging.DEBUG
DEBUG_WINDOW_NAME = 'Dart Recognition'


def draw_corners(corners, img):
    for corner in corners:
        x, y = corner.ravel()
        cv2.circle(img, (x, y), 2, 255, 8)


def write(img, text):
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(
            img,
            text,
            (10, 25),
            font,
            1,
            (255, 255, 255),
            2,
            cv2.LINE_AA)


class Dart (threading.Thread):
    def __init__(self, source, debug=False):
        threading.Thread.__init__(self)
        self.log = logging.getLogger(__name__)
        self.stream = source.get('stream')
        self.mount = source.get('label')
        self.calib_data = source.get('calib_data')
        self.hist = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        self.debug = debug
        self.dart_loc = None

        if debug:
            self.log.setLevel(LOG_DEBUG)
            for handler in self.log.handlers:
                print(handler)
                handler.setLevel(LOG_DEBUG)

    def get_image(self):
        success, img = self.stream.read()

        if not success:
            return (success, img)

        equalized = self.equalize_img(cv2.flip(img, -1))

        return True, equalized

    def run(self):
        non_zero_min_thresh = 2000
        non_zero_max_thresh = 20000
        non_zero_range = range(non_zero_min_thresh, non_zero_max_thresh)
        rect_max_thresh = 50000
        success, ref_img = self.get_image()
        rows, cols = ref_img.shape
        if self.debug:
            self.prev_img = ref_img.copy()

        while success:
            success, comp_img = self.get_image()

            if not success:
                continue

            diff_img = cv2.absdiff(ref_img, comp_img)
            blured = cv2.GaussianBlur(diff_img, (5, 5), 0)
            filtered = cv2.bilateralFilter(blured, 9, 75, 75)
            _, thresh = cv2.threshold(filtered, 60, 255, 0)
            non_zero = cv2.countNonZero(thresh)

            self.log.debug('%s - Non zero count: %s', self.mount, non_zero)
            if non_zero not in non_zero_range:
                self.log.debug(
                    'Image non zero count is %s. Min: %s, Max: %s',
                    non_zero,
                    non_zero_min_thresh,
                    non_zero_max_thresh)
                continue

            # wait for camera vibrations and take a new shot
            time.sleep(0.2)
            success, comp_img = self.get_image()

            if not success:
                self.log.debug('Stream could not be accessed')
                continue

            diff_img = cv2.absdiff(ref_img, comp_img)
            kernel = np.ones((8, 8), np.float32) / 40
            blur = cv2.filter2D(diff_img, -1, kernel)
            corners = self.get_feature_corners(blur)
            # ref_img = comp_img

            if corners is None or corners.size < 40:
                self.log.debug('%s - Dart not recognized. Corner size below 40', self.mount)
                continue

            corners = self.get_mean_corners(corners)

            # dart outside?
            if corners.size < 30:
                self.log.debug('%s - Dart not recognized. Corner size below 30', self.mount)
                continue

            # find left and rightmost corners
            corners = self.get_outermost_corners(corners, cols)

            x, y, w, h = cv2.boundingRect(corners)
            rect = (w * h)

            # filter if rect is over threshhold
            if rect > rect_max_thresh:
                self.log.debug(
                        '%s - Dart not recognized. Bounding rectangle exeeds limit of %s. %s',
                        self.mount,
                        rect_max_thresh, rect)
                continue

            if self.debug:
                self.prev_img = comp_img.copy()
                # write(self.prev_img, 'Rect size: {0}'.format(rect))
                # draw_corners(corners, self.prev_img)
                cv2.rectangle(self.prev_img, (x, y), (x+w, y+h), (0, 255, 0), 1, cv2.LINE_AA)

            if self.mount == "right":
                maxloc = np.argmax(corners, axis=0)
            else:
                maxloc = np.argmin(corners, axis=0)

            locationofdart = corners[maxloc]
            self.dart_loc = locationofdart
            self.log.info('%s - Dart found at location: %s', self.mount, locationofdart)

            if self.debug:
                cv2.circle(self.prev_img, (locationofdart.item(0), locationofdart.item(1)), 10, (255, 255, 255), 2, 8)
                cv2.circle(self.prev_img, (locationofdart.item(0), locationofdart.item(1)), 2, (0, 255, 0), 2, 8)

            ref_img = comp_img

    def equalize_img(self, img):
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        hist_img = self.hist.apply(gray_img)

        return hist_img

    def get_feature_corners(self, img):
        edges = cv2.goodFeaturesToTrack(
                img,
                640,
                0.0008,
                1,
                mask=None,
                blockSize=3,
                useHarrisDetector=1,
                k=0.06)

        if edges is None:
            self.log.debug('Non edges detected')

            return None

        corners = np.int0(edges)
        self.log.debug('%s feature corners detected', len(corners))

        return corners

    def get_mean_corners(self, corners):
        # filter corners
        filter_indizes = []
        mean_corners = np.mean(corners, axis=0)
        for index, point in enumerate(corners):
            xl, yl = point.ravel()
            # filter noise to only get dart arrow
            # threshold important -> make accessible
            if abs(mean_corners[0][0] - xl) > 280:
                filter_indizes.append(index)
            if abs(mean_corners[0][1] - yl) > 220:
                filter_indizes.append(index)

        corners = np.delete(corners, [filter_indizes], axis=0)
        self.log.debug('%s mean corners detected', len(corners))

        return corners

    def get_outermost_corners(self, corners, cols):
        [vx, vy, x, y] = cv2.fitLine(corners, cv2.DIST_HUBER, 0, 0.1, 0.1)
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)

        filter_indizes = []
        for index, point in enumerate(corners):
            xl, yl = point.ravel()
            # check distance to fitted line, only draw corners within certain range
            distance = Math.dist(0, lefty, cols-1, righty, xl, yl)
            if distance > 40:
                filter_indizes.append(index)

        corners = np.delete(corners, [filter_indizes], axis=0)
        self.log.debug('%s outer most corners detected', len(corners))

        return corners
