import math
import cv2
import numpy as np
from .Draw import Draw

cal_l_path = './calibration_data/cal_l.pkl'
cal_r_path = './calibration_data/cal_r.pkl'


def _noop(x):
    pass


def _get_sec_sin(sector_angle, i):
    return math.sin((0.5 + i) * sector_angle)


def _get_sec_cos(sector_angle, i):
    return math.cos((0.5 + i) * sector_angle)


class CalibrationData:
    def __init__(self, mount):
        self.mount = mount
        self.__radius = None
        self.__center = None
        self.__points = None
        self.__matrix = None

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, val):
        self.__radius = int(val)

    @property
    def center(self):
        return self.__center

    @center.setter
    def center(self, val):
        self.__center = tuple(map(int, val))

    @property
    def points(self):
        return self.points

    @points.setter
    def points(self, val):
        self.__points = val

    @property
    def matrix(self):
        return self.__matrix

    @matrix.setter
    def matrix(self, val):
        self.__matrix = val


class Calibration ():
    def __init__(self, source):
        self.stream = source.get('stream')
        self.mount = source.get('mount')
        self.cal_data = CalibrationData(self.mount)

    def start(self):
        _, img = self.stream.read()
        rows, cols, channels = img.shape
        self.shape = (rows, cols)

        self.shape_rev = (cols, rows)
        self.rows = rows
        self.cols = cols
        self.center = (cols / 2, rows / 2)
        self.radius = (min(rows, cols) / 2) - 40

        self.cal_data.radius = self.radius
        self.cal_data.center = self.center
        self.cal_data.points = self.calculate_dst(self.center)
        self.cal_data.matrix = self.get_transformation_point(img)

        return self.cal_data

    def get_transformation_point(self, img):
        win1_name = 'Calibration {0}'.format(self.mount)
        win2_name = 'Preview {0}'.format(self.mount)
        board = Draw()
        im_copy = img.copy()
        dst_points = self.calculate_dst(self.center)
        src_points = [[0, 0], [0, 0], [0, 0], [0, 0]]
        position = 0

        def _storeCoordinates(event, x, y, *args):
            if event is cv2.EVENT_LBUTTONDOWN:
                src_points[position] = [x, y]

        cv2.namedWindow(win1_name)
        cv2.createTrackbar('Position', win1_name, 0, 3, _noop)
        cv2.setMouseCallback(win1_name, _storeCoordinates)
        cv2.imshow(win1_name, img)

        while (1):
            kill = cv2.waitKey(1) & 0xFF
            if kill == 13 or kill == 27:
                break

            points_copy = np.copy(src_points)
            new_position = cv2.getTrackbarPos('Position', win1_name)
            if new_position != position:
                position = new_position
                src_points = np.copy(points_copy)
            matrix = cv2.getPerspectiveTransform(
                np.array(points_copy, np.float32),
                np.array(dst_points, np.float32))
            im_copy = cv2.warpPerspective(img, matrix, self.shape_rev)
            board.drawBoard(im_copy, self.cal_data)
            cv2.imshow(win2_name, im_copy)

        im_copy = cv2.warpPerspective(img, matrix, self.shape_rev)
        cv2.destroyAllWindows()

        return matrix

    def calculate_dst(self, center):
        sector_angle = 2 * math.pi / 20

        '''
            12 = 9/12
            2 = 15/2
            7 = 7/16
            17 = 18/4
        '''

        points = []
        for i in [12, 2, 7, 17]:
            points.append([
                (center[0] + self.radius * _get_sec_cos(sector_angle, i)),
                (center[1] + self.radius * _get_sec_sin(sector_angle, i))
            ])

        return points
