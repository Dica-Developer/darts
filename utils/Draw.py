import cv2
import math

DEBUG = True


class Draw:
    def __init__(self):
        # 20 sectors...
        self.sectorangle = 2 * math.pi / 20

    # improve and make circle radius accessible
    def drawBoard(self, img, cal_data):
        cv2.circle(img, cal_data.center, cal_data.radius, (255, 255, 255), 1)

        for i in range(20):
            dst_cos = math.cos((0.5 + i) * self.sectorangle)
            dst_sin = math.sin((0.5 + i) * self.sectorangle)
            x = int(cal_data.center[0] + cal_data.radius * dst_cos)
            y = int(cal_data.center[1] + cal_data.radius * dst_sin)
            cv2.line(img, cal_data.center, (x, y), (255, 255, 255), 1)

        return img
