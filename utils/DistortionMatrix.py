import numpy as np
import cv2
import yaml

from VideoCapture import VideoStream


# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((13*9, 3), np.float32)
objp[:, :2] = np.mgrid[0:9, 0:13].T.reshape(-1, 2)

# Arrays to store object points and image points from all the images.
objpoints = []  # 3d point in real world space
imgpoints = []  # 2d points in image plane.


def getMatrix(cam, direction):
    found = 0
    while(found < 10):  # Here, 10 can be changed to whatever number you like to choose
        ret, img1 = cam.read()
        gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (9, 13), None)

        # If found, add object points, image points (after refining them)
        if ret:
            objpoints.append(objp)

            corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            # img = cv2.drawChessboardCorners(img1, (9, 13), corners2, ret)
            found += 1

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    data = {'camera_matrix': np.asarray(mtx).tolist(), 'dist_coeff': np.asarray(dist).tolist()}
    with open("calibration/undistort_" + direction + ".yaml", "w") as f:
        yaml.dump(data, f)


def gatherMatrix():
    cam_l = VideoStream(src=1).start()
    cam_r = VideoStream(src=2).start()

    getMatrix(cam_l, "left")
    getMatrix(cam_r, "right")

    cam_l.stop()
    cam_r.stop()


def calibrate(cam_l, mtx, dist):
    ret, img = cam_l.read()
    h,  w = img.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

    # undistort
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

    # mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
    # dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

    # crop the image
    # x,y,w,h = roi
    # dst = dst[y:y+h, x:x+w]

    cv2.namedWindow('Preview')
    cv2.imshow('Preview', dst)

    cv2.moveWindow('Preview', 0, 0)
    while(1):
        kill = cv2.waitKey(1) & 0xFF

        if kill == 13 or kill == 27:
            cv2.destroyAllWindows()
            cam_l.stop()
            break


if __name__ == '__main__':
    gatherMatrix()
