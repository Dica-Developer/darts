from os import path, makedirs
import time

import cv2
import numpy as np

from VideoCapture import VideoStream

timestr = time.strftime("%Y%m%d-%H%M%S")
current_path = path.abspath(path.dirname(__file__))
video_path = path.abspath(path.join(current_path, 'test_videos', timestr))
cam1_path = path.abspath(path.join(video_path, 'cam1.mp4'))
cam2_path = path.abspath(path.join(video_path, 'cam2.mp4'))

if not path.exists(video_path):
    makedirs(video_path)


def takeReferenceVideos():
    try:
        _, img1 = cam1.read()
        _, img2 = cam2.read()
    except Exception as error:
        print('Could not initiate cams: {0}'.format(error))
        return

    cam1_height, cam1_width, _ = img1.shape
    cam2_height, cam2_width, _ = img2.shape

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    cam1_out = cv2.VideoWriter(cam1_path, fourcc, 20.0, (cam1_width, cam1_height))
    cam2_out = cv2.VideoWriter(cam2_path, fourcc, 20.0, (cam2_width, cam2_height))

    while(1):
        _, img_l = cam1.read()
        _, img_r = cam2.read()

        cam1_out.write(img_l)
        cam2_out.write(img_r)

        live_feed = np.hstack((img_l, img_r))

        cv2.imshow('Live', live_feed)
        cv2.moveWindow('Live', 0, 0)

        if (cv2.waitKey(1) & 0xFF) == ord('q'):  # Hit `q` to exit
            break

    cam1.stop()
    cam2.stop()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    global cam1
    global cam2

    cam1 = VideoStream(src=1).start()
    cam2 = VideoStream(src=2).start()

    takeReferenceVideos()
