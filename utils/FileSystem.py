import os
import sys
import logging


log = logging.getLogger()


def getroot():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def joinpath(*paths):
    return os.path.join(*paths)


def abspath(path):
    return os.path.abspath(path)


def existpath(path):
    return os.path.exists(path)


def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as err:
        log.error("OS error: {0}".format(err))
