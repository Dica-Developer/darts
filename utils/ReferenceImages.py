import os
import cv2

from .VideoCapture import Stream


def _joinpath(path):
    return os.path.join(path)


def _abspath(path):
    return os.path.abspath(path)


dirname = os.path.dirname(__file__)
current_path = _abspath(dirname)
img_path = _abspath(_joinpath(current_path, '..', 'test_images'))
img1_path = _abspath(_joinpath(img_path, 'cam1.jpg'))
img2_path = _abspath(_joinpath(img_path, 'cam2.jpg'))


def loadReferenceImages():
    img1_exists = os.path.exists(img1_path)
    img2_exists = os.path.exists(img2_path)

    if not img1_exists or not img2_exists:
        print('Some reference images seems not to exist.')
        return None, None

    img1 = cv2.imread(img1_path)
    img2 = cv2.imread(img2_path)

    return img1, img2


def takeReferenceImages():
    try:
        _, img1 = cam1.read()
        _, img2 = cam2.read()
    except Exception as error:
        print('Could not initiate cams: {0}'.format(error))
        return

    '''
    cv2.imwrite(img1_path, img1)
    cv2.imwrite(img2_path, img2)
    '''
    cam1.stop()
    cam2.stop()


if __name__ == '__main__':
    global cam1
    global cam2

    cam1 = Stream(src=1).start()
    cam2 = Stream(src=2).start()

    takeReferenceImages()
