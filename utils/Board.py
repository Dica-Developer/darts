import cv2
import heapq
import functools
import math
import numpy as np
import logging

LOG_DEBUG = logging.DEBUG

GREEN = 60
SENSITIVITY = 20

LOWER_GREEN = np.array([GREEN - SENSITIVITY, 100, 100])
UPPER_GREEN = np.array([GREEN + SENSITIVITY, 255, 255])
LOWER_RED_0 = np.array([0, 100, 100])
UPPER_RED_0 = np.array([SENSITIVITY, 255, 255])
LOWER_RED_1 = np.array([180 - SENSITIVITY, 100, 100])
UPPER_RED_1 = np.array([180, 255, 255])

CV_WHITE = (255, 255, 255)
CV_BLACK = (0, 0, 0)


class BoardZones:
    def __init__(self, side, **kwargs):
        self.side = side
        self.single = []
        self.double = []
        self.treble = []
        self.bull = []
        self.bullseye = []
        self.noscore = []
        self.center = []
        self.log = logging.getLogger(__name__)

        if kwargs.get('verbose'):
            self.log.setLevel(LOG_DEBUG)
            for handler in self.log.handlers:
                handler.setLevel(LOG_DEBUG)

    def add_zones(self, zones):
        self.single = zones.get('single')
        self.double = zones.get('double')
        self.treble = zones.get('treble')
        self.bull = zones.get('bull')
        self.bullseye = zones.get('bullseye')
        self.noscore = zones.get('noscore')
        self.center = zones.get('center')

    def show_zones(self):
        stack1 = np.hstack((self.noscore, self.single, self.double))
        stack2 = np.hstack((self.treble, self.bull, self.bullseye))
        stack3 = np.vstack((stack1, stack2))

        cv2.imshow('Stack', stack3)
        cv2.moveWindow('Stack', 0, 0)

        while(1):
            kill = cv2.waitKey(1) & 0xFF

            if kill == 13 or kill == 27:
                cv2.destroyAllWindows()
                break


class Recognizer:
    def __init__(self, sources, calib_data, **kwargs):
        self.sources = sources
        self.calib_data = calib_data
        self.log = logging.getLogger(__name__)
        self.verbose = kwargs.get('verbose')

        if self.verbose:
            self.log.setLevel(LOG_DEBUG)
            for handler in self.log.handlers:
                handler.setLevel(LOG_DEBUG)

    def run(self):
        """Kick off finding hit zones on the board

        Iterating over all sources, usually 2, either actual
        cam or a video stream. Step through different processes
        of image preparing, like bluring and undistort.
        """
        self.log.info('Trying to find score zones of board')
        hitzones = {}
        for source in self.sources:
            mount = source.get('mount')
            stream = source.get('stream')
            self.calib = self.calib_data[mount]
            self.log.debug('Find zones of the %s cam/video', mount)
            _, img = stream.read()
            # image shape is (height, width, channels)
            h, w, c = img.shape

            matrix = self.calib.matrix
            prepared_img = cv2.warpPerspective(img.copy(), matrix, (w, h))
            green_red_mask = self.get_mask(prepared_img)
            # with blue eleminated and a B/W mask of just green/red
            # we're able to find the actual hitzone w/o surroundings
            # like numbers and advertisments
            outer_ellipse = self.find_boundary(green_red_mask)
            # find score and noscore zones of board
            score_zone, no_score_zone = self.find_main_zones(outer_ellipse, (h, w, c))
            # find all 5 zones (single, double, treble, bull, bullseye)
            zones = self.find_zones(score_zone, green_red_mask, prepared_img.copy())
            zones['noscore'] = no_score_zone
            self.log.debug('All zones found')

            hitzones[mount] = zones
            '''
            board_zones = BoardZones(side=mount, verbose=self.verbose)
            board_zones.add_zones(zones)

            # segments = self.find_segments(img, board_zones)
            # board_zones.show_zones()
            return board_zones
            '''
        return hitzones

    def prepare_image(self, img, mtx, dist, new_mtx):
        """Returns a prepared image for further processes
        Prepares given image by undistorting and bluring

        Keyword Arguments:
        img -- the image to process
        mtx -- matrix for undistort from stored value
        dist -- distance coefficient for undistort
        new_mtx -- goal matrix for undistort
        """
        self.log.debug('Undistort and blur image for further processes')
        undistort = cv2.undistort(img, mtx, dist, None, new_mtx)
        blur = cv2.GaussianBlur(undistort, (5, 5), 0)

        return blur

    def get_mask(self, img):
        """Returns a B/W mask of green and red segments

        Keyword Arguments:
        img -- prepared image of board
        """
        self.log.debug('Find mask of green and red segments')
        _, green, red = self.split_image(img)
        # combine red and green channels into one image
        green_red = cv2.bitwise_or(red, green)
        green_red_mask = self.get_green_red_mask(green_red)

        return green_red_mask

    def split_image(self, img):
        """Return sepearted BGR channels of an image

        Keyword Arguments:
        img -- The image to extract channels
        """
        # seperate channels from given image
        red = img[:, :, 2]
        green = img[:, :, 1]
        blue = img[:, :, 0]
        channels = []
        # iterate over channels and create an image with just the channel
        for values, channel in zip((red, green, blue), (2, 1, 0)):
            image = np.zeros((values.shape[0], values.shape[1], 3),
                             dtype=values.dtype)
            image[:, :, channel] = values
            channels.append(image)

        return channels[2], channels[1], channels[0]

    def get_green_red_mask(self, img):
        """Returns two B/W masks 1 for green and 1 for red

        Keyword Arguments:
        img -- an image of just green and red channels
        """
        # blank blank image to draw the mask in white
        black = np.ones(img.shape, np.uint8)
        # make a copy of the actual image to not alter this
        working_copy = img.copy()
        hsv = cv2.cvtColor(working_copy, cv2.COLOR_BGR2HSV)
        # the color ranges can vary, maybe make it accessable later on
        green_mask = cv2.inRange(hsv, LOWER_GREEN, UPPER_GREEN)
        # the range of red in HSV crosses the 180° border
        # we need to find both ends, eg. 0°-30° and 150°-180°
        red_mask_1 = cv2.inRange(hsv, LOWER_RED_0, UPPER_RED_0)
        red_mask_2 = cv2.inRange(hsv, LOWER_RED_1, UPPER_RED_1)
        # combine both red masks into one
        red_mask = cv2.bitwise_or(red_mask_1, red_mask_2)
        # find all contours of the seperated masks
        _, green_contours, _ = cv2.findContours(
            green_mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        _, red_contours, _ = cv2.findContours(
            red_mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # draw all found contours in a black image with white
        green = cv2.drawContours(black, green_contours, -1,
                                 (255, 255, 255), cv2.FILLED, cv2.LINE_AA)
        red = cv2.drawContours(black, red_contours, -1,
                               (255, 255, 255), cv2.FILLED, cv2.LINE_AA)

        # comibine back both masks into one
        green_red_mask = cv2.bitwise_or(green, red)

        return green_red_mask

    def find_boundary(self, mask):
        """Returns the outer most ellipse of score zone

        Keyword Arguments:
        mask -- A prepared B/W mask with just green/red channels
        """
        search_for_ellipse = True
        kernel = np.ones((4, 4), np.uint8)
        morph = cv2.Canny(mask, 125, 255)
        THRESHOLD = math.pi * (self.calib.radius * self.calib.radius)
        MIN_THRESHOLD = THRESHOLD - 6000
        MAX_THRESHOLD = THRESHOLD + 6000

        while (search_for_ellipse):
            _, contours, _ = cv2.findContours(
                morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            # top_n_areas = heapq.nlargest(10, contours, _contour_to_area)
            top_result = [contour for contour in contours if _area_threshold(contour, MIN_THRESHOLD, MAX_THRESHOLD)]

            if len(top_result) == 1:
                ellipse = cv2.fitEllipse(top_result[0])
                search_for_ellipse = False

            morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)

        return ellipse

    def find_main_zones(self, ellipse, img_shape):
        black = np.ones(img_shape, np.uint8)
        score_zone_ellipse = _draw_zone(black, ellipse, CV_WHITE)
        _, score_zone = cv2.threshold(
            score_zone_ellipse, 127, 255, cv2.THRESH_BINARY)
        no_score_zone = 255 - score_zone  # mask of no score zone

        return score_zone, no_score_zone

    def find_zones(self, score_zone, green_red_mask, img):
        cropped_mask = cv2.bitwise_and(green_red_mask, score_zone)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
        morphed_mask = cv2.morphologyEx(cropped_mask, cv2.MORPH_CLOSE, kernel)
        rings = cv2.Canny(morphed_mask, 125, 255, cv2.THRESH_BINARY)
        _, ring_contours, _ = cv2.findContours(
            rings, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        black = np.ones(score_zone.shape, np.uint8)
        ellipses = functools.reduce(_reduce_rings, ring_contours, [])

        filtered_ellipses = []
        for index, ellipse in enumerate(ellipses):
            (x, y), (MA, ma), angle = ellipse
            area = int(math.pi / 4 * MA * ma)

            if index > 0:
                _, (prev_MA, prev_ma), _ = ellipses[index - 1]
                prev_area = int(math.pi / 4 * prev_MA * prev_ma)
                accepted_range = range(prev_area - 1000, prev_area)

                print(area)

                if area < 800 or area in accepted_range:
                    continue

            filtered_ellipses.append(((x, y), (MA, ma), angle))

        multi_2 = _draw_zone(black, filtered_ellipses[0], CV_WHITE)
        multi_2 = _draw_zone(multi_2, filtered_ellipses[1], CV_BLACK)
        multi_3 = _draw_zone(black, filtered_ellipses[2], CV_WHITE)
        multi_3 = _draw_zone(multi_3, filtered_ellipses[3], CV_BLACK)
        single_bull = _draw_zone(black, filtered_ellipses[4], CV_WHITE)

        cropped_mask = cv2.bitwise_and(green_red_mask, single_bull)
        bull = cv2.Canny(cropped_mask, 125, 255, cv2.THRESH_BINARY)
        _, bull_contours, _ = cv2.findContours(
            bull, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        top_results = [c for c in bull_contours if _area_range(c, 160, 350)]
        bull_ellipse = cv2.fitEllipse(top_results[0])
        double_bull = _draw_zone(black, bull_ellipse, CV_WHITE)

        single = score_zone.copy()
        single = cv2.bitwise_xor(single, multi_2)
        single = cv2.bitwise_xor(single, multi_3)
        single = cv2.bitwise_xor(single, single_bull)
        single_bull = cv2.bitwise_xor(double_bull, single_bull)
        center = ellipses[-1][0]

        return {
                'single': single,
                'double': multi_2,
                'treble': multi_3,
                'bull': single_bull,
                'bullseye': double_bull,
                'center': center
                }

    def find_segments(self, img, board_zones):
        h, w, _ = img.shape
        win1_name = 'WIN1'
        win2_name = 'WIN2'

        center = [0, 0]
        src_points = [
                [0, 0], [0, 0], [0, 0], [0, 0], [0, 0],
                [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        position = 0

        def _storeCoordinates(event, x, y, params, _):
            if event is cv2.EVENT_LBUTTONDOWN:
                if position == 0:
                    center[0] = x
                    center[1] = y
                else:
                    src_points[position - 1][0] = x
                    src_points[position - 1][1] = y

        cv2.namedWindow(win1_name)
        cv2.createTrackbar('Position', win1_name, 0, 10, _noop)
        cv2.setMouseCallback(win1_name, _storeCoordinates)
        cv2.imshow(win1_name, img)

        while (1):
            kill = cv2.waitKey(1) & 0xFF
            if kill == 13 or kill == 27:
                break

            points_copy = np.copy(src_points)
            new_position = cv2.getTrackbarPos('Position', win1_name)
            if new_position != position:
                position = new_position
                src_points = np.copy(points_copy)

            img_to_show = img.copy()
            for point in src_points:
                p1, p2 = _calculate_line_points(1000, center, point)
                img_to_show = cv2.line(img_to_show, p1, p2, CV_BLACK, 2, cv2.LINE_AA)

            cv2.imshow(win2_name, img_to_show)

        cv2.destroyAllWindows()

        return []


def _show(img):
    cv2.imshow('Preview', img)
    while(1):
        kill = cv2.waitKey(1) & 0xFF

        if kill == 13 or kill == 27:
            cv2.destroyAllWindows()
            break


def _area_threshold(contour, min, max):
    area = cv2.contourArea(contour)

    return min < area < max


def _area_range(contour, min, max):
    area = cv2.contourArea(contour)
    print(area)

    return min < area < max


def _contour_to_area(contour):
    area = cv2.contourArea(contour)
    print(area)
    return cv2.contourArea(contour)


def _reduce_rings(acc, contour):
    if len(contour) > 4:
        acc.append(cv2.fitEllipse(contour))

    return acc


def _draw_zone(img, ellipse, color):
    copy = img.copy()

    return cv2.ellipse(
            copy,
            ellipse,
            color,
            cv2.FILLED,
            cv2.LINE_AA)


def _noop(x):
    pass


def _calculate_line_points(boundary, p1, p2):
    new_p1 = [0, 0]
    new_p2 = [boundary, 0]
    # Check if the line is a vertical line because vertical lines don't have slope
    if (p1[0] != p2[0]):
        slope = (p1[1] - p2[1]) / (p1[0] - p2[0])
        line = p1[1] - (slope * p1[0])
        new_p1[1] = slope * new_p1[0] + line
        new_p2[1] = slope * new_p2[0] + line
    else:
        new_p1 = [p1[0], 0]
        new_p2 = [p1[0], boundary]

    return tuple(map(int, new_p1)), tuple(map(int, new_p2))
