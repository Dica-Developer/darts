#!/usr/bin/env python

"""This script helps to setup and/or check prerequisites"""

import argparse
import logging
import logging.config
import pickle
import sys
import yaml
import utils.FileSystem as fs
import utils.Video as Video
# import utils.Calibration

with open('config.yaml', 'rt') as file:
    CONFIG = yaml.safe_load(file.read())

logging.config.dictConfig(CONFIG.get('logger'))

LOG = logging.getLogger('Setup')

PARSER = argparse.ArgumentParser(
    description='Steeldart Camera',
    formatter_class=argparse.RawTextHelpFormatter)

PARSER.add_argument(
    'mode',
    choices=['calibrate', 'undistort'],
    help="""
        Choose the part to setup.
        Is it you're first time setup it is recommended to do them `all`.
        [all] - Do all required setup steps. (Might require …)
        [calibrate] - Opens calibration UI and recognizes board hitzones.
        [undistort] - Uses a chessboard pattern to eliminate camera distortions""")

PARSER.add_argument(
    '--verbose',
    '-v',
    action='store_true',
    help='Make setup chatty')

PARSER.add_argument(
    '--dry',
    action='store_true',
    help='No changes to filesystem or configuration will be made')

PARSER.add_argument(
    '--noCam',
    action='store_true',
    help="""
        This is for development purposes only.
        Enable this option to use the sample videos
        instead of actual cameras.
        """)

PARSER.add_argument(
    '--force',
    '-f',
    action='store_true',
    help="""
        If enabled all previous calibration data and board hitzones
        are rejected and run again
        """)

ARGS = PARSER.parse_args()
DRY_RUN = ARGS.dry
NO_CAM = ARGS.noCam
FORCE_RUN = ARGS.force
ROOT_PATH = fs.getroot()

if ARGS.verbose:
    LEVEL = logging.DEBUG
    LOG.setLevel(LEVEL)
    for handler in LOG.handlers:
        handler.setLevel(LEVEL)

    LOG.debug('Logging level verbose')


if DRY_RUN:
    LOG.info('Dry run …')


if FORCE_RUN:
    LOG.warning('Running with "--force" flag. I really hope you know what you\'re doing')

SOURCES = []

if NO_CAM:
    videos = CONFIG.get('videos')

    for video in videos:
        src = fs.joinpath(ROOT_PATH, video.get('src'))
        mount = video.get('label')
        stream = Video.File(src=src).start()

        SOURCES.append({'stream': stream, 'mount': mount, 'src': src})
else:
    cams = CONFIG.get('cams')

    for cam in cams:
        src = cam.get('src')
        mount = cam.get('label')
        stream = Video.Stream(src=src).start()

        SOURCES.append({'stream': stream, 'mount': mount, 'src': src})


def _check_streams(streams):
    ok = True

    for source in streams:
        src = source.get('src')
        stream = source.get('stream')

        LOG.debug('Check video file %s', src)
        try:
            ok, _ = stream.read()
        except Exception as error:
            ok = False
            LOG.error('Video source %s seems not available', src)
            LOG.erro('Unexpected error: %s', error)
            raise

        if not ok:
            LOG.error('Video source %s seems not available', src)
            stream.stop()
            break

    return ok


def precondition_check():
    """
    Checks if required folder are available
    other create them.
    Checks if the cameras are available and
    setup correctly.
    precondition_check() -> Boolean
    """

    # Check existence of required folder
    for folder in CONFIG.get('folder'):
        path = fs.joinpath(ROOT_PATH, folder)
        LOG.debug('Check existencs of: "%s"', path)

        if not fs.existpath(path):
            LOG.debug('Folder: "%s" not found.', path)
            if not DRY_RUN:
                fs.mkdir(path)
                LOG.debug('Folder: "%s" created.', path)

    return _check_streams(SOURCES)


def prepare_board_hitzones():
    import utils.Board as Board

    img_sources = []
    calib_data = {}

    calibration_path = fs.joinpath(ROOT_PATH, 'calibration')

    for side in ['left', 'right']:
        src = fs.joinpath(calibration_path, 'calib_{side}.pkl'.format(side=side))
        hitzone_path = fs.joinpath(calibration_path, 'hitzones_{0}.pkl'.format(side))

        if not fs.existpath(hitzone_path) or FORCE_RUN:
            with open(src, 'rb') as file:
                calib_data[side] = pickle.load(file)
        else:
            LOG.info('Hitzone data for {0} found. Skipping recognition'.format(side))

    if len(calib_data) > 0:
        recognizer = Board.Recognizer(sources=SOURCES, calib_data=calib_data, verbose=ARGS.verbose)
        hit_zone_masks = recognizer.run()
        for k, v in hit_zone_masks.items():
            with open('{0}/hitzones_{1}.pkl'.format(calibration_path, k), 'wb') as f:
                pickle.dump(v, f)

        for source in img_sources:
            stream = source.get('stream')
            stream.stop()


def calibrate():
    from utils.Calibration import Calibration

    cal_path = fs.joinpath(ROOT_PATH, 'calibration')

    for source in SOURCES:
        mount = source.get('mount')
        path = fs.joinpath(cal_path, 'calib_{0}.pkl'.format(mount))

        if not fs.existpath(path) or FORCE_RUN:
            cal = Calibration(source=source)
            cal_data = cal.start()

            with open(path, 'wb') as f:
                pickle.dump(cal_data, f, pickle.HIGHEST_PROTOCOL)
        else:
            LOG.info('Calibration data for {0} found. Skipping calibration'.format(mount))


LOG.info('Setup started in "%s" mode.', ARGS.mode)
LOG.info('Precondition check …')
check_ok = precondition_check()

if not check_ok:
    LOG.error('Preconditions are not ok. Please check the log')
    sys.exit(1)
else:
    LOG.info('Precondition check is OK')

if ARGS.mode == 'calibrate':
    LOG.info('calibrate')
    calibrate()
    prepare_board_hitzones()
elif ARGS.mode == 'undistort':
    LOG.info('all')
else:
    LOG.info('all')
