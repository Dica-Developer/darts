#!/usr/bin/env python

import argparse
import cv2
import logging
import logging.config
import Dart
import numpy as np
import pickle
import yaml
import utils.Draw
import utils.FileSystem as fs
import utils.Video as Video

with open('config.yaml', 'rt') as file:
    CONFIG = yaml.safe_load(file.read())

logging.config.dictConfig(CONFIG.get('logger'))

LOG_DEBUG = logging.DEBUG
LOG = logging.getLogger('Darts')
ROOT_PATH = fs.getroot()

PARSER = argparse.ArgumentParser(
    description='Steeldart Camera',
    formatter_class=argparse.RawTextHelpFormatter)

PARSER.add_argument(
        '--noCam',
        action='store_true')

PARSER.add_argument(
        '--debug',
        action='store_true')

ARGS = PARSER.parse_args()
DEBUG = ARGS.debug
NO_CAM = ARGS.noCam

print(DEBUG)

if DEBUG:
    LOG.setLevel(LOG_DEBUG)
    for handler in LOG.handlers:
        handler.setLevel(LOG_DEBUG)

    LOG.debug('Debug logging enabled')


darts = []


if NO_CAM:
    videos = CONFIG.get('videos')

    for video in videos:
        src = fs.joinpath(ROOT_PATH, video.get('src'))
        label = video.get('label')
        calib_path = fs.joinpath(ROOT_PATH, 'calibration', 'calib_{0}.pkl'.format(label))
        with open(calib_path, 'rb') as f:
            calib_data = pickle.load(f)

        stream = Video.File(src=src).start()
        source = {'stream': stream, 'label': label, 'calib_data': calib_data}
        dart = Dart.Dart(source=source, debug=DEBUG)
        dart.start()
        darts.append(dart)

else:
    cams = CONFIG.get('cams')

    for cam in cams:
        src = cam.get('src')
        label = cam.get('label')
        stream = Video.Stream(src=src).start()
        calib_path = fs.joinpath(ROOT_PATH, 'calibration', 'calib_{0}.pkl'.format(label))
        with open(calib_path, 'rb') as f:
            calib_data = pickle.load(f)

        source = {'stream': stream, 'label': label, 'calib_data': calib_data}
        dart = Dart.Dart(source=source, debug=DEBUG)
        dart.start()
        darts.append(dart)

if DEBUG:
    cv2.namedWindow('Debug')
    board = utils.Draw.Draw()

    def getTransformedLocation(loc, matrix):
        # transform only the hit point with the saved transformation matrix
        # ToDo: idea for second camera -> transform complete image and overlap both images to find dart location?
        dart_loc_temp = np.array([[loc.item(0), loc.item(1)]], dtype="float32")
        dart_loc_temp = np.array([dart_loc_temp])
        dart_loc = cv2.perspectiveTransform(dart_loc_temp, matrix)
        new_dart_loc = tuple(dart_loc.reshape(1, -1)[0])

        return new_dart_loc

    while(1):
        kill = cv2.waitKey(1) & 0xFF

        if kill == 13 or kill == 27:
            cv2.destroyAllWindows()
            break

        dart_loc_1 = darts[0].dart_loc
        dart_loc_2 = darts[1].dart_loc

        cal_data_1 = darts[0].calib_data
        cal_data_2 = darts[1].calib_data
        try:
            prev_img_1 = darts[0].prev_img
            prev_img_2 = darts[1].prev_img

            preview_1 = np.hstack((prev_img_1, prev_img_2))
            cv2.imshow('Debug', preview_1)
        except:
            print('error')

        rows, cols = prev_img_1.shape
        black = np.ones((600, 800), np.uint8)
        preview = board.drawBoard(black, cal_data_1)

        if dart_loc_1 is not None:
            matrix_1 = cal_data_1.matrix
            new_dart_loc_1 = getTransformedLocation(dart_loc_1, matrix_1)
            cv2.circle(preview, new_dart_loc_1, 10, (255, 255, 255), 2, 8)
            cv2.circle(preview, new_dart_loc_1, 2, (0, 255, 0), 2, 8)

        if dart_loc_2 is not None:
            matrix_2 = cal_data_2.matrix
            new_dart_loc_2 = getTransformedLocation(dart_loc_2, matrix_2)
            cv2.circle(preview, new_dart_loc_2, 10, (255, 255, 255), 2, 8)
            cv2.circle(preview, new_dart_loc_2, 2, (0, 255, 0), 2, 8)

        cv2.imshow('Board', preview)
